# University of York Thesis Template

This git repository contains LaTeX source that implements the
Univesity of York rules for formatting of theses. The full list of
requirements and recommendations can be found
[here](https://www.york.ac.uk/research/graduate-school/academic/thesis/format).

# Pre-requsites

- Some kind of (La)TeX environment with access to commands like `pdflatex` and `bibtex` - probably /
  hopefully you have this already

- `latexmk` - **this will probably have to be installed if you haven't installed it yourself
  previously** as most Linux distributions do not have it installed by default (hint: try `sudo apt
install latexmk` or equivalent)

- usual stuff like `make`, a pdf viewer, a text editor, etc - you almost certainly have these
  already

# Usage

## Overview

To use this repository, the steps are as follows:

1. Download the git repository

2. Ensure the pre-requsites are installed

3. Select the version you require

4. Add your own LaTeX customisations

5. Write you thesis!

Each of these steps is discussed in more detail below.

## Download the git repository

`git clone
https://gitlab.com/stephen-biggs-fox/uoy-thesis-template.git`

Alternatively, you may wish to fork [the
project](https://gitlab.com/stephen-biggs-fox/uoy-thesis-template) on
gitlab.com. Then you can keep you thesis under version control (you
should do this anyway) and maintain a remote backup with simply `git
push`.

Unfortunately, custom template repositories are a premium feature of
gitlab.com.

## Ensure the pre-requsites are installed

Most linux distributions come with LaTeX already installed. If it's
not there already, then you need to install it. I don't know how to do
that becuase it was already installed on my system. (Try asking the
internet!) You need access to commands like `pdflatex` and `bibtex`.

One package that I did have to install was `latexmk`. This avoids
having to re-compile everything multiple times whenever you make a
tiny change which will save a lot of waiting around, especially on a
large document like this. I have a debian derivative so the command
was:

`sudo apt install latexmk`

## Checkout the tagged version you require

Different versions are available depending on how much of the guidance
you want to follow. If you're not sure which one to use, check each
one out in turn and make it to see how it will look.

- minimal - implements the minimal set of requirements (title page,
  abstract, etc.). Only mandatory requirements are implemented - no
optional extras are included. Select this for maximum flexibility in
your own customisation.

- standard - same as minimal plus a small number of the recommended
  formatting optional extras that are expected to be useful in most
cases (bibliography support, graphics support, acknowledgements
section, etc.). This is a more practical starting point than 'minimal'
but still offers a high degree of flexibility in your own
customisation.

- full - same as standard plus this version has all but the most
  pedantic of recommendations implemented. (Included: 1.5 line
spacing, recommended page / float numbering, appendix support, most
recommended sections. Not included: abbreviations, nomenclature, and
index sections.) Select this version if you want to follow most of the
recommendations, including most of the optional extras.

- pedantic - same as full but with all recommendations implemented
  except indexing which is expected to be unnecessary in most cases,
i.e. abbreviations and nomenclature sections are now included. Select
this version if you need to use either of those sections.

- really-pedantic - same as pedantic but with
  indexing support. Select this version if you need indexing.

## Add your own LaTeX customisations

Everyone has their own way of presenting documents, whether it's your
favourite font, specially selected margins and line spacing, or a
particular bibliography style. The LaTeX provided by this repository
is intended to be a starting point that implements the university
requirements / recommendations; you may wish to add your own stuff to
make it look how you want, to make the writing process quicker
with custom macros, or to make managing the document easier with a
custom directory structure.

## Write your thesis

Definitely do not forget to do this bit!

# How to make thesis

A `Makefile` is provided. The files have been set up in such a way
that you should be able to type `make thesis` (or just `make`) and
LaTeX will only do the necessary steps in the compilation process
(i.e. only compile files that have changed). If using the minimal
version or implementing your own directory structure, you will have to
update the Makefile to ensure that all files (e.g. bibliography and
image files) are included as dependencies in the right places.

# Acknowledgements

Many thanks to Thomas Nicholas for providing useful feedback during
beta testing of this template.

# License

Copyright (c) 2019 Stephen Biggs-Fox
 
Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
